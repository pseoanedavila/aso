#!/bin/bash

#Arguments received:
#$1: Course name
#$2: Absolute path containing student accounts
#3: Absolute path to store assignments

handleError() 
{
	echo "$(date "+%d/%m/%y %H:%M") $1" >> prac.log
}

if ! [ -d $3 ]
then
	mkdir $3
fi

for dir in $(ls $2)
do
	if [ -r $2/$dir/prac.sh ]
	then 
		cp $2/$dir/prac.sh $3/"$dir".sh
	else 
		handleError "Cannot read $2/$dir/prac.sh"
	fi
done