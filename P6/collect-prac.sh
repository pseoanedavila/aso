#!/bin/bash
printHeader() 
{
    echo "Aso 19/20 - Assignment 6"
    echo "Student name: Paulo Seoane Davila"
    echo
    echo "Practical assignment management"
    echo "------------------------------"
    echo
}

programTask()
{
	# DAY=$(date -v +1d +%d)
	# MONTH=$(date -v +1d +%m)
	# echo "0 8 $DAY $MONTH * $*" > task 
 #    crontab task 2>> prac.log 
 	declare -i tomorrow
 	DAY=$(date +%d)
 	tomorrow=$DAY+1
 	MONTH=$(DATE +%m)
 	echo "0 8 $DAY $MONTH * $*" > task 
    crontab task 2>> prac.log 
}


printMenu() 
{
    echo "Menu"
    echo "1) Program collection of assignment solutions"
    echo "2) Pack course assignments"
    echo "3) See size and date of a course backup file"
    echo "4) End program"
    read -p "Option: " option
}

#Arguments:
#($1 = Message: String.)
handleError() 
{
    echo "$1"
	echo "$(date "+%d/%m/%y %H:%M") $1" >> prac.log
}

#Main functions

pack() 
{
    CONTINUE=-1
    while [ "$CONTINUE" != "y" -a "$CONTINUE" != "n" ]
    do
        echo "Menu 2 - Pack Course Assignments"
        read -p "Course: " COURSE
        read -p "Absolute path containing the directory with the assignments: " ASSPATH
        if [ -d $ASSPATH ]
        then
            if [ "$COURSE" = "" -o "$ASSPATH" = "" ]
            then
                handleError "The course and the path to store assignments cannot be empty"
            else 
                echo "The assignments in the directory $ASSPATH will be packed"
                read -p "Do you want to continue? (y/n) " CONTINUE
            fi
        else 
            handleError "The directory $ASSPATH does not exist"
        fi
    done

    if [ $CONTINUE = "y" ]
    then 
        if [ -d $ASSPATH -a -r $ASSPATH -a -w $ASSPATH ]
        then
            FILES=$(ls $ASSPATH)
            tar -czf $ASSPATH/"$COURSE-$(date +%d%m%y).tgz" -C $ASSPATH $FILES
        else
            handleError "Cannot pack files in $ASSPATH"
        fi
    fi
}

store() 
{   
    CONTINUE=-1
    while [ "$CONTINUE" != "y" -a "$CONTINUE" != "n" ] 
    do
	    echo "Menu 1 - Program collection of assignment solutions"
	    echo
	    read -p "Course: " COURSE
	    read -p "Absolute path containing student accounts: " ACCPATH
	    read -p "Absolute path to store assignments: " ASSPATH

    	if [ -d $ACCPATH ]
    	then
            if [ "$COURSE" = "" -o "$ASSPATH" = "" ]
            then
                handleError "The course and the path to store assignments cannot be empty"
            else
        	   read -p "Do you want to continue? (y/n) " CONTINUE
            fi
        else 
        	handleError "The directory $ACCPATH does not exist" 
        fi
    done
    
    if [ $CONTINUE = y ] 
    then
        programTask $PWD/store-prac.sh $COURSE $ACCPATH $ASSPATH 
        echo "The $COURSE assignment collection process is programmed for tomorrow at 8:00. Origin $ACCPATH. Destination: $ASSPATH"
    fi
    
}

getInfo()
{
    read -p "Course: " COURSE
    FILE=$(find . -name "$COURSE-*.tgz") 2>> prac.log
    if [ "$FILE" = "" ] 
    then 
        handleError "No backup of the course $COURSE was found"
    else 
        if [ ! -r $FILE ]
        then 
            handleError "Unable to get information of $FILE. Check it's permisions"
        else 
            SIZE=$(ls -l $FILE |  tr -s ' ' | cut -d ' ' -f 5) 2>> prac.log
            DAY=$(ls -l $FILE |  tr -s ' ' | cut -d ' ' -f 6) 2>> prac.log
            MONTH=$(ls -l $FILE |  tr -s ' ' | cut -d ' ' -f 7) 2>> prac.log
            HOUR=$(ls -l $FILE |  tr -s ' ' | cut -d ' ' -f 8) 2>>prac.log
            echo "The file generated is $(basename $FILE). It's size is $SIZE and it was generated on $DAY $MONTH $HOUR"
        fi
    fi
}


processOption() 
{
    case "$1" in
    1) 
        store 
        ;;
    2) 
        pack
        ;;  
    3) 
        getInfo
        ;;
    4) 
        exit 0 
        ;;
    *) 
        echo "Invalid option" 
        ;;
    esac
}

main() 
{
    option=-1
    printHeader
    while [ true ]
    do
        printMenu
        processOption $option
    done
}

main
